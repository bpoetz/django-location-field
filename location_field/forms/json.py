from django.forms import fields
from jsonfield.fields import JSONCharFormField

from location_field.widgets import LocationWidget


class JSONLocationField(JSONCharFormField):
    def __init__(self, based_fields=None, zoom=None, suffix='', default=None,
                 *args, **kwargs):
        kwargs['initial'] = default

        self.widget = LocationWidget(based_fields=based_fields, zoom=zoom,
                                     suffix=suffix, **kwargs)

        dwargs = {
            'required': True,
            'label': None,
            'initial': None,
            'help_text': None,
            'error_messages': None,
            'show_hidden_initial': False,
        }

        for attr in dwargs:
            if attr in kwargs:
                dwargs[attr] = kwargs[attr]

        super(JSONLocationField, self).__init__(*args, **dwargs)

    def to_python(self, value):
        lat, lng = value.split(',')
        return {u'lat': lat, u'lng': lng}


