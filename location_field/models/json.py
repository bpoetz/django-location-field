from django.db.models import CharField
from jsonfield.fields import JSONCharField
from location_field.forms.json import JSONLocationField
from location_field.models.base import BaseLocationField


class JSONLocationField(BaseLocationField, JSONCharField):
    formfield_class = JSONLocationField

    def __init__(self, based_fields=None, zoom=None, suffix='',
                 max_length=250, *args, **kwargs):

        super(JSONLocationField, self).__init__(based_fields=based_fields,
                                                 zoom=zoom, suffix=suffix, *args, **kwargs)

        JSONCharField.__init__(self, max_length=max_length, *args, **kwargs)


# south compatibility
try:
    from south.modelsinspector import add_introspection_rules
    add_introspection_rules([], ["^location_field\.models\.json\.JSONLocationField"])
except:
    pass
